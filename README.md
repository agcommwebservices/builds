# UK Ag Comm build files

This is a collection of drush make files. They get fed to Aegir and act as stubs
to bootstrap the platform creation process.


## Release Notes
- **08/August/2014**:
    - Updated README.md for formatting and to add release notes section.
    - Updated coa.build to use [Drupal 7.31][1] to mitigate a
      [Security Issue][2].
    - Updated counties-d6.build to use [Drupal 6.33][3] to mitigate a
      [Security Issue][2].

[3]: https://www.drupal.org/drupal-6.33-release-notes "View 6.33 Release notes"
[2]: https://www.drupal.org/SA-CORE-2014-004 "View Security Announcement SA-CORE-2014-004"
[1]: https://www.drupal.org/drupal-7.31-release-notes "View 7.31 Release notes"
