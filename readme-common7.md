## Synopsis

This build file species all of the projects that are common to all of our 
install profiles.

## Updating this file

Updates to this build will be included in all future platforms, regardless of 
the install profile used. **_Modules in sites/all/modules will override modules
in the install profile._** 

**NOTE:** If projects are removed from the global build they must be disabled 
and uninstalled from any sites prior to that site being migrated to a platform 
built using this build file.  Aegir _should_ warn you of this during the 
migration confirmation

## Release Notes

- *15/July/2014*: 
  - Added in COA as an additional install profile to support migrations.
  - Adds module Override Node Options. (https://www.drupal.org/project/override_node_options)
  - Updates module Features Override reference. (https://www.drupal.org/project/features_override) 
- *14/July/2014*: Initial version.

